//
//  ViewController.swift
//  MoMo_App
//
//  Created by Macbook on 6/11/20.
//  Copyright © 2020 Macbook. All rights reserved.
//

import UIKit
import Alamofire

class ViewController: UIViewController {
    
    @IBOutlet weak var tb_main: UITableView!
    
    var data: [UserData] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        tb_main.rowHeight = UITableView.automaticDimension
        self.getData()
    }

    func getData() {
        AF.request("https://randomuser.me/api/?seed=1&page=1&results=20").responseJSON{ (response) in
            let result = response.result
            do {
                let resultObject = try result.get()
                if let json = resultObject as? NSDictionary {
                    if let results = json.value(forKey: "results") as? [NSDictionary] {
                        debugPrint(results[0])
                        for item in results {
                            let itemData = UserData()
                            if let name = item.value(forKey: "name") as? NSDictionary {
                                let first = name.value(forKey: "first") as! String
                                let last = name.value(forKey: "last") as! String
                                let title = name.value(forKey: "title") as! String
                                itemData.username = title + " " + first + " " + last
                            }
                            if let picture = item.value(forKey: "picture") as? NSDictionary {
                                let large = picture.value(forKey: "large") as! String
                                itemData.avatarUrl = large
                            }
                            self.data.append(itemData)
                        }
                    }
                    self.tb_main.reloadData()
                }
            } catch  {
                
            }
            
        }
    }

}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MainCell", for: indexPath) as! MainTableViewCell
        let item = data[indexPath.row]
        cell.updateData(name: item.username, url: item.avatarUrl)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

class UserData: NSObject {
    var username: String = ""
    var avatarUrl: String = ""
}

