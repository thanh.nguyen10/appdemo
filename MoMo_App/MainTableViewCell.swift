//
//  MainTableViewCell.swift
//  MoMo_App
//
//  Created by Macbook on 6/11/20.
//  Copyright © 2020 Macbook. All rights reserved.
//

import UIKit
import SDWebImage

class MainTableViewCell: UITableViewCell {
    
    @IBOutlet weak var img_avatar: UIImageView!
    @IBOutlet weak var lb_name: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updateData(name: String, url: String) {
        img_avatar.loadImageWithUrl(url: url)
        lb_name.text = name
    }

}

extension UIImageView {
    func loadImageWithUrl(url: String) {
        self.sd_setImage(with: URL(string: url), placeholderImage: UIImage(named: "default_avatar"))
    }
}
